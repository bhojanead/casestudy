var shelljs = require('shelljs');
//var docx = require('docx');

exports.config = {
  environment: 'prod',
  host: 'localhost',
  port: 4444,
  path: '/wd/hub',

  specs: [
    // './tests/acceptance/wdiopo/features/OpenWeatherHomePage/OpenWeatherHomePage.1.feature',
    './tests/acceptance/wdiopo/features/OpenWeatherHomePage/OpenWeatherHomePage.2.feature',
    // './tests/acceptance/wdiopo/features/OpenWeatherHomePage/OpenWeatherHomePage.3.feature',
    // './tests/acceptance/wdiopo/features/OpenWeatherHomePage/OpenWeatherHomePage.4.feature',
  ],


  maxInstances: 1,
  deprecationWarnings: false,

  /**********************************************************************************************************
   ********************************************   Capabilities  **********************************************
   ***********************************************************************************************************/

  capabilities: [{
    browserName: 'chrome',
    pageLoadStrategy: 'normal',
  }],

  /**********************************************************************************************************
  ********************************************   Test Configurations  ***************************************
  ***********************************************************************************************************
    Define all options that are relevant for the WebdriverIO instance here
   Level of logging verbosity: silent | verbose | command | data | result | error */

  logLevel: 'error',
  debug: true,
  sync: true,
  waitforTimeout: 60000,

  framework: 'cucumber',

  cucumberOpts: {
    require: ['./tests/acceptance/wdiopo/features/stepDefinitions/*.steps.js'],
    backtrace: true,
    compiler: ['js:babel-core/register'],
    timeout: 60000,
    strict: true,
    colors: true,
    failAmbiguousDefinitions: true,
    ignoreUndefinedDefinitions: false,
    format: ['pretty'],
    tagExpression: '@Test1 or @Test2 or @Test3 or @Test4'
  },

  services: ['selenium-standalone'],
  seleniumLogs: 'tests/acceptance/wdiopo/output/',

  before: function before() {
    const chai = require('chai');
    global.expect = chai.expect;
    global.assert = chai.assert;
    global.should = chai.should();
  },

  afterStep: function (stepResult) {

    if (stepResult.status == "failed") {
      console.log(("\t FAILED Step : " + stepResult.step.name).error);
      console.log(("\t FAILED Step at line: " + stepResult.step.line).error);

      var getStepName = encodeURIComponent(stepResult.step.name.replace(/\s+/g, ''));
      var timestamp = new Date().toJSON().replace(/:/g, '-');
      var filename = 'STEPFAILED_LINE' + stepResult.step.line + '_' + getStepName + '_' + timestamp;
      var filePath = './tests/acceptance/wdiopo/output/screenshots/failedSteps/' + filename + '.png';

      browser.saveScreenshot(filePath);
    }
   },

  beforeScenario: function () {

  },

  afterScenario: function () {
    browser.reload();
  },

};