﻿var gulp = require('gulp');
var path = require('path');
var exec = require('child_process').exec;
var wdio = require('gulp-wdio');
var shell = require('gulp-shell');
var cache = require('gulp-cache');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');

var isWin = /^win/.test(process.platform);
var cmd = isWin ? 'wdio.cmd' : 'wdio';
var wdioBin = path.join(__dirname, 'node_modules', '.bin', cmd);

gulp.task('clearCache', function () {
  gulp.src('tests/acceptance/wdiopo/*.js')
    .pipe(cache.clear());

  // Or, just call this for everything
  //cache.clearAll();
});

gulp.task('clear', () =>
  cache.clearAll()
);

gulp.task('clean', function () {
  return gulp.src('app/tmp', {
      read: false
    })
    .pipe(clean());
});

gulp.task('test', ['clearCache','clear','clean'],(err) => {
  var wdioConf = 'wdio.conf.js';
  var wd = wdio({
    type: 'selenium',
    wdio: {},
    wdioBin,
  });
  return gulp.src(wdioConf).pipe(wd)
  .on('error',  function  (err) {
    console.log(err.toString());
    return  done(err);
  })
});


gulp.task('default', ['acceptance']);

var seleniumServer;
gulp.task('selenium', function () {
  console.log('In Selenium');
  var Chromeexecutable = isWin ?
    'node_modules/chromedriver/lib/chromedriver/chromedriver.exe' :
    'node_modules/chromedriver/bin/chromedriver';

  var IEexecutable = isWin ?
    'node_modules/iedriver/lib/iedriver/IEDriverServer.exe' :
    'node_modules/iedriver/bin/iedriver';

  var command = isWin ?
    'java -jar node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-2.53.0.jar -log ./seleniumLog.txt -Dwebdriver.ie.driver=' +
    path.resolve(__dirname, IEexecutable) +
    ' -Dwebdriver.chrome.driver=' +
    path.resolve(__dirname, Chromeexecutable) :
    'java -jar node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-2.53.0.jar -log ./seleniumLog.txt -Dwebdriver.chrome.driver=' +
    path.resolve(__dirname, Chromeexecutable);

  seleniumServer = exec(command, function () {
    seleniumServer = null;
  });
});

