import _reduce from 'lodash/reduce';
import Utils from '../helpers/utils';

const fs = require("fs");
const path = require("path");
const yaml = require('js-yaml');
const rootPath = path.join(__dirname, '..');
const pagesList = yaml.load(fs.readFileSync('tests/acceptance/wdiopo/helpers/pages.yml'));


const featurePages = _reduce(pagesList, (pages, pageName) => ({
  ...pages,
  [pageName]: require(`${rootPath}/features/${pageName}/${pageName}.page`).default,
}), {});


let currentPage;

export const getPage = name => featurePages[name];
export const getCurrentPage = () => getPage(currentPage);
export const setCurrentPage = name => {
  currentPage = name;
  // console.info(`===> current page - ${name} <===`);
  
};