import {
  getPage,
  getCurrentPage,
  setCurrentPage
} from '../helpers/currentPage';
import _reduce from 'lodash/reduce';
import Utils from '../helpers/utils';

var now = require("performance-now");

const path = require("path");
const rootPath = path.join(__dirname, '..');
var mergeYaml = require('merge-yaml');

var _map = require('lodash/map');
var fs = require("fs");
var yaml = require('js-yaml');
var environmentData = yaml.load(fs.readFileSync('tests/acceptance/wdiopo/envconfig/env.dev.yml'));
var testData = yaml.load(fs.readFileSync('tests/acceptance/wdiopo/data/data.dev.yml'));
var locatorsArrayList = yaml.load(fs.readFileSync('tests/acceptance/wdiopo/features/locators/locatorsArrayList.yml'));

var fieldSelector;

var mergedLocatorsYaml = mergeYaml([
  `${rootPath}/features/locators/OpenWeatherHomePage.locators.yml`,
  `${rootPath}/features/locators/OpenWeatherResultPage.locators.yml`,
]);


var mergedContentYaml = mergeYaml([
  `${rootPath}/features/OpenWeatherHomePage/OpenWeatherHomePage.content.yml`,
  `${rootPath}/features/OpenWeatherResultPage/OpenWeatherResultPage.content.yml`,
]);


class Page {

  getBaseURL(page) {
    try {
      var envData = environmentData['prod'];
      var baseURL = envData[page];
      return baseURL;
    } catch (e) {
      if (e.name == "TypeError")
        console.log('************ Please make sure your tests are mapped to correct environment ******************'.error);
    }
  }

  open(path) {
    try {
      browser.url(path);
    } catch (e) {
      console.log(('************ Please make sure URL is correct and application is up and running ******************' + e.message)).error;
    }
  }


  getElement(selector) {
    try {
      var selectorValue = mergedLocatorsYaml[selector];
      if (!browser.isVisible(selectorValue) && !browser.isExisting(selectorValue)) {
        browser.waitUntil(() => {
          return browser.element(selectorValue).isVisible()
        }, 5000, "Element is not visible")
        if (!browser.isVisible(selectorValue)) throw new Error(
          "Can't find a " + selector + " selector with the value " + selectorValue + " in " + getCurrentPage() + " page after a wait of 5 sec"
        );
      }
      return browser.element(selectorValue);
    } catch (e) {
      console.log(("************ getElement function is bypassed for the " + selector + " selector with the value " + selectorValue + " in " + getCurrentPage() + " ,Please make sure it is identified ******************" + e.message).error);
    }
  }

  load(page) {
    this.open(this.getBaseURL(page));
    browser.windowHandleMaximize();
  }

  consider(sectionData, dataFile) {
    try {
      if (dataFile == 'testDataFile')
        this.specificData = testData[sectionData];
      else if (dataFile == 'contentDataFile')
        this.specificData = mergedContentYaml[sectionData];
    } catch (e) {
      console.log(("************ " + dataFile + " consideration for the section data " + sectionData + " is NOT successful ").error);
    }
  }


  browserWait(milliseconds) {
    browser.pause(milliseconds);
  }

  WaitForLoad(action, element, targetVal, attribute) {
    fieldSelector = this.getElement(element);
    var state = fieldSelector.getAttribute(attribute);
    browser.waitUntil(function () {
      return fieldSelector.getAttribute(attribute) === mergedContentYaml[targetVal]
    }, 5000, 'Element text is not displayed after 5 sec');
  }

  explicitWait(fieldSelector) {
    try {
      if ((fieldSelector.getText().length) == 0) {
        browser.waitUntil(function () {
          return fieldSelector.getText().length > 0
        }, 1000, 'Element text is not displayed after 1 sec');
      } else if (fieldSelector.getText() == '(0)') {
        browser.waitUntil(function () {
          return fieldSelector.getText() != '(0)'
        }, 5000, 'Element text is not displayed after 2 sec');
      }
    }
    catch (e) {
      console.log(("************ explicitWait function is bypassed for the " + fieldSelector + fieldSelector.getText() + getCurrentPage() + " ,Please make sure it is identified ******************" + e.message).error);
    }
  }

  ModifiedsingleAction(action, locator, detail) {
    try {
      if (action == 'verify') {
        this.specificData = mergedContentYaml[detail];
        fieldSelector = this.getElement(locator);
        this.explicitWait(fieldSelector);
        var assertResult = assert.equal((fieldSelector.getText()),
          this.specificData, 'Data validation check for the ' + detail + ' is failed');
        console.log(('\n' + detail + " validation for the " + detail + " with the value " + fieldSelector.getText() + " on the page is Success").info);
      } else if (action == 'click') {
        fieldSelector = this.getElement(locator);
        fieldSelector.click();
      } else if (action == 'clear') {
        fieldSelector = this.getElement(locator);
        fieldSelector.clearElement();
      } else if (action == 'enter') {
        fieldSelector = this.getElement(locator);
        fieldSelector.setValue(this.specificData[detail]);
      } else if (action == "hover") {
        var hoverValue = mergedLocatorsYaml[detail]
        browser.moveToObject(hoverValue);
      }
    } catch (e) {
      if (e.name)
        console.log(("********* Inside singleAction method ************ " + action + " action is NOT successful for " + detail + " with the value " + fieldSelector.getText()).error);
      console.log("Exception message", e.message);
    }
  }


  ModifiedmultipleActions(action, locator, details) {
    var j = 0;
    _map(
      locatorsArrayList[locator],
      function (searchFieldKey) {
        try {
          if (action == 'select') {
            var theArray = this.specificData[details].split(",");
            for (var i = 0; i < theArray.length; i++) {
              if (theArray[i].length > 0) {
                if (theArray[i] == searchFieldKey) {
                  fieldSelector = this.getElement(searchFieldKey);
                  browser.pause(500);
                  fieldSelector.click();
                }
              }
            }
          } else if (action == 'verify') {
            this.specificData = mergedContentYaml[details];
            fieldSelector = this.getElement(searchFieldKey);
            this.explicitWait(fieldSelector);
            var assertResult = assert.equal((fieldSelector.getText()),
              this.specificData[searchFieldKey], 'Data validation check for the ' + searchFieldKey + ' is failed');
            console.log(('\n' + details + " validation for the " + searchFieldKey + " with the value " + fieldSelector.getText() + " on the page is Success").info);
          } else if (action == 'validate') {
            this.specificData = mergedContentYaml[details];
            var verifyArray = this.specificData.split(",");
            if (verifyArray[j].length > 0) {
              fieldSelector = this.getElement(searchFieldKey);
              this.explicitWait(fieldSelector);
              var assertResult = assert.equal((fieldSelector.getText()),
                verifyArray[j], 'Data validation check for the ' + searchFieldKey + 'is failed');
              console.log(('\n' + details + " validation for the " + searchFieldKey + " with the value " + fieldSelector.getText() + " on the page is Success").info);
              j++;
            }
          } else if (action == 'enter') {
            fieldSelector = this.getElement(searchFieldKey);
            fieldSelector.setValue(this.specificData[searchFieldKey]);
          } else if (action == 'hover') {
            fieldSelector = this.getElement(searchFieldKey);
            browser.moveToObject(fieldSelector);
          } else if (action == 'click') {
            fieldSelector = this.getElement(searchFieldKey);
            fieldSelector.click();
          }
        } catch (e) {
          if (e.name)
            console.log(("********* Inside multipleActions method ************ " + action + " action is NOT successful for " + searchFieldKey + " with the value " + fieldSelector.getText()).error);
          console.log("Exception message", e.message);
        }
      }.bind(this)
    );
  }




}

export default new Page();