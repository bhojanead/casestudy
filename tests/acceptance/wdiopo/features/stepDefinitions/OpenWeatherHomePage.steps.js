import { Given, When, Then } from 'cucumber';
import Page from '../../helpers/page';
import Utils from '../../helpers/utils';
import { getPage, getCurrentPage, setCurrentPage } from '../../helpers/currentPage';

Given(/^I am on the \"([^\"]*)\" page$/, function (pageName) {
    Page.load(pageName);
    getPage(pageName);
    setCurrentPage(pageName);
});

Then(/^I consider \"([^\"]*)\" from \"([^\"]*)\"$/, function (sectionData,dataFile) {
    Page.consider(sectionData,dataFile);
});

Then(/^I should be on \"([^\"]*)\" page$/, function (pageName) {
    setCurrentPage(pageName);
    browser.pause(2000);
});



  Then(/^I \"([^\"]*)\" \"([^\"]*)\" with \"([^\"]*)\" value$/, function (action, locator, data) {
    Page.ModifiedsingleAction(action,locator,data);
});


  Then(/^I \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$/, function (action, locator, dummy) {
    if(dummy=="button"){Page.ModifiedsingleAction(action,locator,dummy);} //click action
    else if(dummy=="values"){Page.ModifiedsingleAction(action,locator,dummy);} //Clear action
    else{Page.ModifiedmultipleActions(action,locator,dummy);} //multiple click
  });


  Then(/^I \"([^\"]*)\" \"([^\"]*)\" field contains \"([^\"]*)\" value$/, function (action, locator, content) {
    Page.ModifiedsingleAction(action,locator,content);
  });
  

  Then(/^I \"([^\"]*)\" \"([^\"]*)\" fields contain \"([^\"]*)\" values$/, function (action, locator, content) {
    Page.ModifiedmultipleActions(action,locator,content);
  });


