Feature: Second end-to-end test
    As a user on the Open Weather Map site, I want to Enter Invalid Credentials and verify the result

    Background:
        Given I am on the "OpenWeatherHomePage" page

    @Test4
    Scenario Outline: Login "<UserData>"
        When I consider "<UserData>" from "testDataFile"
        And I "click" "HP_SignInLink" "Link"
        Then I "enter" "LoginDetails" with "Login" values
        When I "click" "Submit" "button"
        And I "verify" "Failure" field contains "Failure" values

        Examples: City to search
            | UserData |
            | User 1   | 

