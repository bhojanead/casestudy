Feature: Second end-to-end test
    As a user on the Open Weather Map site, I want to Enter Invalid City Name and Verifies the result

    Background:
        Given I am on the "OpenWeatherHomePage" page

    @Test2
    Scenario Outline: Search "<City Name>"
        When I consider "<City Name>" from "testDataFile"
        Then I "enter" "YourCityNameTxt" with "YourCityNameTxt" value
        When I "click" "SearchButtonTop" "button"
        Then I should be on "OpenWeatherResultPage" page
        And I "verify" "Result Unsucess Summary" fields contain "<Weather Information>" values

        Examples: City to search
            | City Name | Weather Information   |
            | City 1    | City 1 Result Summary |

